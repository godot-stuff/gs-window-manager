extends Node2D

const DESIRED_RESOLUTION = Vector2(320, 180)


func _ready():
	self.get_viewport().size_changed.connect(on_root_vp_size_change)
#	self.get_viewport().connect("size_changed", self, "on_root_vp_size_change")
	self.on_root_vp_size_change()

func on_root_vp_size_change():
	var scales = self.get_viewport().size / self.DESIRED_RESOLUTION
	var scaling_factor = floor(min(scales[0], scales[1]))
	var actual_resolution = DESIRED_RESOLUTION * scaling_factor
	$Control/ViewportContainer.margin_top = -actual_resolution[1] / 2
	$Control/ViewportContainer.margin_bottom = actual_resolution[1] / 2
	$Control/ViewportContainer.margin_left = -actual_resolution[0] / 2
	$Control/ViewportContainer.margin_right = actual_resolution[0] / 2
	var default_transform = Transform2D(Vector2(1, 0), Vector2(0, 1), Vector2())
	$Control/ViewportContainer/Viewport.canvas_transform = default_transform.scaled(Vector2(scaling_factor, scaling_factor))
